#!

from math import *

class GeneralFormula:
    def CreateDelta(self, a, b, c):
        return (b*b)-4*a*c
    def CreateP(self, a, b):
        return -b/(2*a)
    def CreateQ(self, delta, a):
        return -delta/(4*a)
    def CreateXList(self, delta, a, b):
        if delta>0: return self.XforDeltaGreater(delta, a, b)
        elif delta==0: return self.XforDeltaEqual(delta, a, b)
        else: return []
    def XforDeltaGreater(self, delta, a, b):
        xFirst = (-b-sqrt(delta))/(2*a)
        xSecond = (-b+sqrt(delta))/(2*a)
        return [xFirst, xSecond]
    def XforDeltaEqual(self, delta, a, b):
        xSolo = -b/(2*a)
        return [xSolo]
    def __init__(self, a, b, c):
        delta = self.CreateDelta(a, b, c)
        p = self.CreateP(a, b)
        q = self.CreateQ(delta, a)
        xList = self.CreateXList(delta, a, b)
        self.PrintReturns(delta, p, q, xList)
    def PrintReturns(self, delta, p, q, xList):
        print('Delta: '+str(delta))
        print('P: '+str(p))
        print('Q: '+str(q))
        print('Miejsca zerowe: ', end='')
        print(xList)

a = int(input('a: '))
b = int(input('b: '))
c = int(input('c: '))
GeneralFormula(a, b, c)

