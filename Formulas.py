#!
import matplotlib.pyplot as plt
import numpy as np
from math import *

class GeneralFormula:
    def CreateDelta(self, a, b, c):
        return (b*b)-4*a*c
    def CreateP(self, a, b):
        return -b/(2*a)
    def CreateQ(self, delta, a):
        return -delta/(4*a)
    def CreateXList(self, delta, a, b):
        if delta>0: return self.XforDeltaGreater(delta, a, b)
        elif delta==0: return self.XforDeltaEqual(delta, a, b)
        else: return []
    def CreateYAxis(self,x, a, b, c):
        return (a * (x ** 2)) + (b * x) + c
    def XforDeltaGreater(self, delta, a, b):
        xFirst = (-b-sqrt(delta))/(2*a)
        xSecond = (-b+sqrt(delta))/(2*a)
        return [xFirst, xSecond]
    def XforDeltaEqual(self, delta, a, b):
        xSolo = -b/(2*a)
        return [xSolo]
    def __init__(self, isGoing, a, b, c):
        if (isGoing):
            delta = self.CreateDelta(a, b, c)
            p = self.CreateP(a, b)
            q = self.CreateQ(delta, a)
            xList = self.CreateXList(delta, a, b)
            self.DrawGraph(a, b, c, p, q, xList)
    def DrawGraph(self, a, b, c, p, q, xList):
        ifListEmpty, distanceFromZero = p + 2, [abs(i) for i in xList]
        if not xList == []: ifListEmpty=max(distanceFromZero)
        scaleOfXAxis = ifListEmpty + 2
        xAxis = np.linspace(-scaleOfXAxis, scaleOfXAxis, 100)
        yAxis = self.CreateYAxis(xAxis, a, b, c)

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.spines['left'].set_position('center')
        ax.spines['bottom'].set_position('zero')
        ax.spines['right'].set_color('none')
        ax.spines['top'].set_color('none')
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')

        plt.plot(xAxis, yAxis, 'r',
        label='Wierzchołek: {' + str(p) + ',' + str(q) + '}' + '\n'
        + 'Miejsca Zerowe: {0}'.format(xList))
        plt.legend(loc='upper left')
        plt.show()

class ProductFormula:
    def __init__(self, isGoing, a, p, q):
        if (isGoing):
            b = self.CreateB()
            c = self.CreateC()
            delta = self.CreateDelta()
            xList = self.CreateXList()
            self.DrawGraph()




